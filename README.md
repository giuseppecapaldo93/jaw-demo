# JAW demo

Project that makes it possible to test the JAW project's functionality. The goal is to create a single project package with a Simple Example. The executable of which use the JAAPI, JAW features make able to see the outcome straight in Accerciser or Orca.

## Before build

Go on [Koji Fedora](https://koji.fedoraproject.org/) and search these elements:

1. atk-2.33.3-1.fc31.x86_64.rpm
2. atk-devel-2.33.3-1.fc31.x86_64.rpm
3. at-spi2-atk-2.33.2-1.fc31.x86_64.rpm
4. at-spi2-atk-devel-2.33.2-1.fc31.x86_64.rpm
5. at-spi2-core-2.33.2-1.fc31.x86_64.rpm
6. at-spi2-core-devel-2.33.2-1.fc31.x86_64.rpm

install these rpm with dnf.

### After check these package in case install it

```
sudo dnf install gobject-introspection-devel
```

## Build And Run

```
./build_and_run
```

## Clean installation

```
./clean
```
